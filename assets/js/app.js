var map, featureList, ursSearch = [], orgsSearch = [], municipiosSearch = [], biomasSearch = [], redesSearch = [];

$(window).resize(function() {
  sizeLayerControl();
});

$(document).on("click", ".feature-row", function(e) {
  $(document).off("mouseout", ".feature-row", clearHighlight);
  sidebarClick(parseInt($(this).attr("id"), 10));
});

if ( !("ontouchstart" in window) ) {
  $(document).on("mouseover", ".feature-row", function(e) {
    highlight.clearLayers().addLayer(L.circleMarker([$(this).attr("lat"), $(this).attr("lng")], highlightStyle));
  });
}

$(document).on("mouseout", ".feature-row", clearHighlight);

$("#about-btn").click(function() {
  $("#aboutModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#rede-btn").click(function() {
  $("#redeModal" + $("#rede-btn").attr('rede-id')).modal("show");
  $("#featureModal").modal("hide");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#legend-btn").click(function() {
  $("#legendModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#login-btn").click(function() {
  $("#loginModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#list-btn").click(function() {
  animateSidebar();
  return false;
});

$("#nav-btn").click(function() {
  $(".navbar-collapse").collapse("toggle");
  return false;
});

$("#sidebar-toggle-btn").click(function() {
  animateSidebar();
  return false;
});

$("#sidebar-hide-btn").click(function() {
  animateSidebar();
  return false;
});

function animateSidebar() {
  $("#sidebar").animate({
    width: "toggle"
  }, 350, function() {
    map.invalidateSize();
  });
}

function sizeLayerControl() {
  $(".leaflet-control-layers").css("max-height", $("#map").height() - 50);
}

function clearHighlight() {
  highlight.clearLayers();
}

function sidebarClick(id) {
  var layer = markerClusters.getLayer(id);
  if ((layer.feature.geometry.type == 'Polygon') || (layer.feature.geometry.type == 'MultiPolygon')) {
    map.setView([layer.getBounds().getCenter().lat, layer.getBounds().getCenter().lng], 8);
  } else {
    map.setView([layer.getLatLng().lat, layer.getLatLng().lng], 8);
  }
  layer.fire("click");
  /* Hide sidebar and go to the map on small screens */
  if (document.body.clientWidth <= 767) {
    $("#sidebar").hide();
    map.invalidateSize();
  }
}

function syncSidebar() {
  /* Empty sidebar features */
  $("#feature-list tbody").empty();
  /* Loop through theaters layer and add only features which are in the map bounds */
  urs.eachLayer(function (layer) {
    if (map.hasLayer(ursLayer)) {
      if (map.getBounds().contains(layer.getLatLng())) {
        $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/URs.svg"></td><td class="feature-name">' + layer.feature.properties.tipo + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      }
    }
  });
  /* Loop through theaters layer and add only features which are in the map bounds */
  orgs.eachLayer(function (layer) {
    if (map.hasLayer(orgsLayer)) {
      if (map.getBounds().contains(layer.getLatLng())) {
        $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/URs.svg"></td><td class="feature-name">' + layer.feature.properties.tipo + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      }
    }
  });
  /* Loop through municipios layer and add only features which are in the map bounds */
  municipios.eachLayer(function (layer) {
    if (map.hasLayer(municipiosLayer)) {
      if (map.getBounds().contains(layer.getBounds())) {
        $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/municipios.svg"></td><td class="feature-name">' + layer.feature.properties.municipios + " (" + layer.feature.properties.municipios_UF + ")" + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      }
    }
  });
  /* Loop through biomas layer and add only features which are in the map bounds */
  biomas.eachLayer(function (layer) {
    if (map.hasLayer(biomasLayer)) {
      if (map.getBounds().contains(layer.getBounds())) {
        $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/biomas.svg"></td><td class="feature-name">' + layer.feature.properties.nom_bioma + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      }
    }
  });
  /* Loop through redes layer and add only features which are in the map bounds */
  redes.eachLayer(function (layer) {
    if (map.hasLayer(redesLayer)) {
      if (map.getBounds().contains(layer.getLatLng())) {
        $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/redes.svg"></td><td class="feature-name">' + layer.feature.properties.nome + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      }
    }
  });
  /* Update list.js featureList */
  featureList = new List("features", {
    valueNames: ["feature-name"]
  });
  featureList.sort("feature-name", {
    order: "asc"
  });
}

/* Basemap Layers */
var cartoLight = L.tileLayer("https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png", {
  maxZoom: 19,
  attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="https://cartodb.com/attributions">CartoDB</a>'
});
var usgsImagery = L.layerGroup([L.tileLayer("http://basemap.nationalmap.gov/arcgis/rest/services/USGSImageryOnly/MapServer/tile/{z}/{y}/{x}", {
  maxZoom: 15,
}), L.tileLayer.wms("http://raster.nationalmap.gov/arcgis/services/Orthoimagery/USGS_EROS_Ortho_SCALE/ImageServer/WMSServer?", {
  minZoom: 16,
  maxZoom: 19,
  layers: "0",
  format: 'image/jpeg',
  transparent: true,
  attribution: "Aerial Imagery courtesy USGS"
})]);

/* Overlay Layers */
var highlight = L.geoJson(null);
var highlightStyle = {
  stroke: false,
  fillColor: "#00FFFF",
  fillOpacity: 0.7,
  radius: 10
};

/* Single marker cluster layer to hold all clusters */
var markerClusters = new L.MarkerClusterGroup({
  spiderfyOnMaxZoom: true,
  showCoverageOnHover: false,
  zoomToBoundsOnClick: true//  disableClusteringAtZoom: 7
});

/* Empty layer placeholder to add to layer control for listening when to add/remove URs to markerClusters layer */
var ursLayer = L.geoJson(null);
var urs = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: "assets/img/URs.svg",
        iconSize: [24, 28],
        iconAnchor: [12, 28],
        popupAnchor: [0, -25]
      }),
      title: feature.properties.categoria,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = "<table class='table table-striped table-bordered table-condensed'>" + "<tr><th>Rede</th><td>" + feature.properties.rede + "</td></tr>" + "<tr><th>Município</th><td>" + feature.properties.municipio + "</td></tr>" + "<tr><th>Tipo</th><td>" + feature.properties.tipo + "</td></tr>" + "<table>";
      layer.on({
        click: function (e) {
          $("#feature-titleURORG").html(feature.properties.categoria);
          $("#feature-infoURORG").html(content);
          $("#featureModalURORG").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/URs.svg"></td><td class="feature-name">' + layer.feature.properties.tipo + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      ursSearch.push({
        name: layer.feature.properties.categoria,
        address: layer.feature.properties.tipo,
        source: "Urs",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/URs.geojson", function (data) {
  urs.addData(data);
});

/* Empty layer placeholder to add to layer control for listening when to add/remove orgs to markerClusters layer */
var orgsLayer = L.geoJson(null);
var orgs = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: "assets/img/orgs.svg",
        iconSize: [24, 28],
        iconAnchor: [12, 28],
        popupAnchor: [0, -25]
      }),
      title: feature.properties.categoria,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = "<table class='table table-striped table-bordered table-condensed'>" + "<tr><th>Rede</th><td>" + feature.properties.rede + "</td></tr>" + "<tr><th>Município</th><td>" + feature.properties.municipio + "</td></tr>" + "<tr><th>Tipo</th><td>" + feature.properties.tipo + "</td></tr>" + "<table>";
      layer.on({
        click: function (e) {
          $("#feature-title").html(feature.properties.categoria);
          $("#feature-info").html(content);
          $("#featureModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/URs.svg"></td><td class="feature-name">' + layer.feature.properties.tipo + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      orgsSearch.push({
        name: layer.feature.properties.categoria,
        address: layer.feature.properties.tipo,
        source: "Orgs",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/orgs.geojson", function (data) {
  orgs.addData(data);
});

/* Empty layer placeholder to add to layer control for listening when to add/remove municipios to markerClusters layer */
var municipiosLayer = L.geoJson(null);
var municipios = L.geoJson(null, {
  style: function (feature) {
    // console.log(feature.properties.municipios_REDE);
    var c = stringToColour(feature.properties.municipios_REDE);
    if (feature.properties.municipios_REDE == "Rede de Comercialização Solidária de Agricultores Familiares e Extrativistas do Cerrado") {
      c = '#A5DB95';
    } else if (feature.properties.municipios_REDE == "Rede de Agricultura Biodinâmica") {
      c = '#B87FB1';
    } else if (feature.properties.municipios_REDE == "Morro da Cutia") {
      c = "#A34ECB";
    }
    return {
        color: c,
        opacity: 1,
        weight: 1.5
    }
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = "<table class='table table-striped table-bordered table-condensed'>" + "<tr><th>Município</th><td>" + feature.properties.NM_MUNICIP + "</td></tr>" + "<tr><th>Estado</th><td>" + feature.properties.municipios_UF + "</td></tr>" + "<tr><th>Rede</th><td>" + feature.properties.municipios_REDE + "</td></tr>" + "<table>";
      layer.on({
        click: function (e) {
          $("#feature-titleURORG").html(feature.properties.municipios + " (" + feature.properties.municipios_UF + ")");
          $("#feature-infoURORG").html(content);
          $("#featureModalURORG").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/municipios.svg"></td><td class="feature-name">' + layer.feature.properties.NM_MUNICIP + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      municipiosSearch.push({
        name: layer.feature.properties.NM_MUNICIP,
        address: layer.feature.properties.ADRESS1,
        source: "Municipios",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/Municipios.geojson", function (data) {
  municipios.addData(data);
  map.addLayer(municipiosLayer);
});

/* Empty layer placeholder to add to layer control for listening when to add/remove biomas to markerClusters layer */
var biomasLayer = L.geoJson(null);
var biomas = L.geoJson(null, {
  style: function (feature) {
    return {
      color: stringToColour(feature.properties.nom_bioma),
      stroke: false,
      fillOpacity: 0.3,
    }
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = "<table class='table table-striped table-bordered table-condensed'>" + "<tr><th>Bioma</th><td>" + feature.properties.nom_bioma + "</td></tr>" + "<tr><th>Área</th><td>" + feature.properties.val_area_k + "</td></tr>" + "<tr><th>Descrição</th><td>" + feature.properties.ds_sintese + "</td></tr>" + "<table>";
      layer.on({
        click: function (e) {
          $("#feature-title").html(feature.properties.nom_bioma);
          $("#feature-info").html(content);
          $("#featureModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/municipios.svg"></td><td class="feature-name">' + layer.feature.properties.NM_MUNICIP + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      biomasSearch.push({
        name: layer.feature.properties.nom_bioma,
        address: layer.feature.properties.ADRESS1,
        source: "Biomas",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/MMA_biomas.geojson", function (data) {
  biomas.addData(data);
});

/* Empty layer placeholder to add to layer control for listening when to add/remove redes to markerClusters layer */
var redesLayer = L.geoJson(null);
var redes = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.divIcon({
        html: "<p>" + feature.properties.sigla + "</p>",
        className: "redesMarker",
        iconSize: [150, 28],
        iconAnchor: [100, 28],
        popupAnchor: [0, -25]
      }),
      title: feature.properties.nome,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = "<table class='table table-striped table-bordered table-condensed'>"
      + "<tr><th>Nome da Rede</th><td>" + feature.properties.nome + "</td></tr>"
      + "<tr><th>Fundação</th><td>" + feature.properties.fundacao + "</td></tr>"
      + "<tr><th>Organizações</th><td>" + feature.properties.n_organizacoes + "</td></tr>"
      + "<tr><th>Unidades de Referência</th><td>" + feature.properties.n_urs + "</td></tr>"
      + "<tr><th>Tecnologias sociais sistematizadas</th><td>" + feature.properties.ts + "</td></tr>"
      + "</table>";
      var openNetworkPage = '<button type="button" class="btn btn-default" data-dismiss="modal">Abrir Página da Rede</button>';
      layer.on({
        click: function (e) {
          $("#feature-title").html(feature.properties.nome);
          $("#feature-info").html(content);
          $("#rede-btn").attr("rede-id",feature.id);
          $("#featureModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/redes.svg"></td><td class="feature-name">' + layer.feature.properties.nome + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      redesSearch.push({
        name: layer.feature.properties.nome,
        address: layer.feature.properties.ADRESS1,
        source: "Redes",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/AER_redes.geojson", function (data) {
  redes.addData(data);
  map.addLayer(redesLayer);
});

map = L.map("map", {
  zoom: 5,
  center: [-15.706406, -48.012288],
  layers: [cartoLight, markerClusters, highlight],
  zoomControl: false,
  attributionControl: false
});

/* Layer control listeners that allow for a single markerClusters layer */
map.on("overlayadd", function(e) {
  if (e.layer === ursLayer) {
    markerClusters.addLayer(urs);
    syncSidebar();
  }
  if (e.layer === orgsLayer) {
    markerClusters.addLayer(orgs);
    syncSidebar();
  }
  if (e.layer === municipiosLayer) {
    markerClusters.addLayer(municipios);
    syncSidebar();
  }
  if (e.layer === biomasLayer) {
    markerClusters.addLayer(biomas);
    syncSidebar();
  }
  if (e.layer === redesLayer) {
    markerClusters.addLayer(redes);
    markerClusters.addLayer(municipios);
    syncSidebar();
  }
});

map.on("overlayremove", function(e) {
  if (e.layer === ursLayer) {
    markerClusters.removeLayer(urs);
    syncSidebar();
  }
  if (e.layer === orgsLayer) {
    markerClusters.removeLayer(orgs);
    syncSidebar();
  }
  if (e.layer === municipiosLayer) {
    markerClusters.removeLayer(municipios);
    syncSidebar();
  }
  if (e.layer === biomasLayer) {
    markerClusters.removeLayer(biomas);
    syncSidebar();
  }
  if (e.layer === redesLayer) {
    markerClusters.removeLayer(redes);
    markerClusters.removeLayer(municipios);
    syncSidebar();
  }
});

/* Filter sidebar feature list to only show features in current map bounds */
map.on("moveend", function (e) {
  syncSidebar();
});

/* Clear feature highlight when map is clicked */
map.on("click", function(e) {
  highlight.clearLayers();
});

/* Attribution control */
function updateAttribution(e) {
  $.each(map._layers, function(index, layer) {
    if (layer.getAttribution) {
      $("#attribution").html((layer.getAttribution()));
    }
  });
}
map.on("layeradd", updateAttribution);
map.on("layerremove", updateAttribution);

var attributionControl = L.control({
  position: "bottomright"
});
attributionControl.onAdd = function (map) {
  var div = L.DomUtil.create("div", "leaflet-control-attribution");
  div.innerHTML = "<span class='hidden-xs'>Developed by <a href='http://bryanmcbride.com'>bryanmcbride.com</a> | </span><a href='#' onclick='$(\"#attributionModal\").modal(\"show\"); return false;'>Attribution</a>";
  return div;
};
map.addControl(attributionControl);

map.options.maxZoom = 10;

var zoomControl = L.control.zoom({
  position: "bottomright"
}).addTo(map);

/* GPS enabled geolocation control set to follow the user's location */
var locateControl = L.control.locate({
  position: "bottomright",
  drawCircle: true,
  follow: true,
  setView: true,
  keepCurrentZoomLevel: true,
  markerStyle: {
    weight: 1,
    opacity: 0.8,
    fillOpacity: 0.8
  },
  circleStyle: {
    weight: 1,
    clickable: false
  },
  icon: "fa fa-location-arrow",
  metric: false,
  strings: {
    title: "My location",
    popup: "You are within {distance} {unit} from this point",
    outsideMapBoundsMsg: "You seem located outside the boundaries of the map"
  },
  locateOptions: {
    maxZoom: 18,
    watch: true,
    enableHighAccuracy: true,
    maximumAge: 10000,
    timeout: 10000
  }
}).addTo(map);

/* Larger screens get expanded layer control and visible sidebar */
if (document.body.clientWidth <= 767) {
  var isCollapsed = true;
} else {
  var isCollapsed = false;
}

var baseLayers = {
  "Mapa": cartoLight,
  "Satélite": usgsImagery
};

var groupedOverlays = {
  "Camadas": {
    "<img src='assets/img/redes.svg' width='24' height='28'>&nbsp;Redes": redesLayer,
    "<img src='assets/img/URs.svg' width='24' height='28'>&nbsp;Unidades de Referência": ursLayer,
    "<img src='assets/img/orgs.svg' width='24' height='28'>&nbsp;Organizações": orgsLayer,
    "<img src='assets/img/biomas.svg' width='24' height='28'>&nbsp;Biomas": biomasLayer,
  }
};

var layerControl = L.control.groupedLayers(baseLayers, groupedOverlays, {
  collapsed: isCollapsed
}).addTo(map);

/* Highlight search box text on click */
$("#searchbox").click(function () {
  $(this).select();
});

/* Prevent hitting enter from refreshing the page */
$("#searchbox").keypress(function (e) {
  if (e.which == 13) {
    e.preventDefault();
  }
});

$("#featureModal").on("hidden.bs.modal", function (e) {
  $(document).on("mouseout", ".feature-row", clearHighlight);
});

/* Typeahead search functionality */
$(document).one("ajaxStop", function () {
  $("#loading").hide();
  sizeLayerControl();
  featureList = new List("features", {valueNames: ["feature-name"]});
  featureList.sort("feature-name", {order:"asc"});

  var ursBH = new Bloodhound({
    name: "Urs",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: ursSearch,
    limit: 10
  });
  var orgsBH = new Bloodhound({
    name: "Orgs",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: orgsSearch,
    limit: 10
  });
  var municipiosBH = new Bloodhound({
    name: "Municipios",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: municipiosSearch,
    limit: 10
  });

  var biomasBH = new Bloodhound({
    name: "Biomas",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: biomasSearch,
    limit: 10
  });

  var redesBH = new Bloodhound({
    name: "Redes",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: redesSearch,
    limit: 10
  });

  var geonamesBH = new Bloodhound({
    name: "GeoNames",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: "http://api.geonames.org/searchJSON?username=bootleaf&featureClass=P&maxRows=5&countryCode=US&name_startsWith=%QUERY",
      filter: function (data) {
        return $.map(data.geonames, function (result) {
          return {
            name: result.name + ", " + result.adminCode1,
            lat: result.lat,
            lng: result.lng,
            source: "GeoNames"
          };
        });
      },
      ajax: {
        beforeSend: function (jqXhr, settings) {
          settings.url += "&east=" + map.getBounds().getEast() + "&west=" + map.getBounds().getWest() + "&north=" + map.getBounds().getNorth() + "&south=" + map.getBounds().getSouth();
          $("#searchicon").removeClass("fa-search").addClass("fa-refresh fa-spin");
        },
        complete: function (jqXHR, status) {
          $('#searchicon').removeClass("fa-refresh fa-spin").addClass("fa-search");
        }
      }
    },
    limit: 10
  });
  orgsBH.initialize();
  ursBH.initialize();
  municipiosBH.initialize();
  biomasBH.initialize();
  redesBH.initialize();
  geonamesBH.initialize();

  /* instantiate the typeahead UI */
  $("#searchbox").typeahead({
    minLength: 3,
    highlight: true,
    hint: false
  }, {
    name: "Urs",
    displayKey: "name",
    source: ursBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/URs.svg' width='24' height='28'>&nbsp;Unidades de Referência</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
  }, {
    name: "Orgs",
    displayKey: "name",
    source: orgsBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/URs.svg' width='24' height='28'>&nbsp;Organizações</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
  }, {
    name: "Municipios",
    displayKey: "name",
    source: municipiosBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/municipios.svg' width='24' height='28'>&nbsp;Municipios</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
  }, {
    name: "Biomas",
    displayKey: "name",
    source: biomasBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/biomas.svg' width='24' height='28'>&nbsp;Biomas</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
  },{
    name: "Redes",
    displayKey: "name",
    source: redesBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/redes.svg' width='24' height='28'>&nbsp;Redes</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
  }, {
    name: "GeoNames",
    displayKey: "name",
    source: geonamesBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/globe.png' width='25' height='25'>&nbsp;GeoNames</h4>"
    }
  }).on("typeahead:selected", function (obj, datum) {
    if (datum.source === "Urs") {
      if (!map.hasLayer(ursLayer)) {
        map.addLayer(ursLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "Orgs") {
      if (!map.hasLayer(orgsLayer)) {
        map.addLayer(orgsLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "Municipios") {
      if (!map.hasLayer(municipiosLayer)) {
        map.addLayer(municipiosLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "Biomas") {
      if (!map.hasLayer(biomasLayer)) {
        map.addLayer(biomasLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "Redes") {
      if (!map.hasLayer(redesLayer)) {
        map.addLayer(redesLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "GeoNames") {
      map.setView([datum.lat, datum.lng], 14);
    }
    if ($(".navbar-collapse").height() > 50) {
      $(".navbar-collapse").collapse("hide");
    }
  }).on("typeahead:opened", function () {
    $(".navbar-collapse.in").css("max-height", $(document).height() - $(".navbar-header").height());
    $(".navbar-collapse.in").css("height", $(document).height() - $(".navbar-header").height());
  }).on("typeahead:closed", function () {
    $(".navbar-collapse.in").css("max-height", "");
    $(".navbar-collapse.in").css("height", "");
  });
  $(".twitter-typeahead").css("position", "static");
  $(".twitter-typeahead").css("display", "block");
});

// Leaflet patch to make layer control scrollable on touch browsers
var container = $(".leaflet-control-layers")[0];
if (!L.Browser.touch) {
  L.DomEvent
  .disableClickPropagation(container)
  .disableScrollPropagation(container);
} else {
  L.DomEvent.disableClickPropagation(container);
}

var stringToColour = function(str) {
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  var colour = '#';
  for (var i = 0; i < 3; i++) {
    var value = (hash >> (i * 8)) & 0xFF;
    colour += ('00' + value.toString(16)).substr(-2);
  }
  return colour;
}
